start:
fence			
addi x1,x0,8		# n = 0

init:
mv x4,x1			# get n
li x5,0				# sum = 0

loop:
bgeu x0,x4,start	# if n == 0 goto start
add x5,x5,x4		# sum = sum + n
addi x4,x4,-1		# n--
j loop
