module Main where

import Lib
import Emulator.Core
import System.Environment
import System.IO

import Tui

main :: IO ()
main = tuiMain

{-
main :: IO ()
main = do  
    path <- head <$> getArgs
    convertInstructions path
-}
