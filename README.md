# Riscvemu

This is a Haskell project that tries to simulate a subset of the RISC-V RV32I ISA.

To build the project, use the `stack` build tool.

You can either import the emulator to run it directly 
or run it with the built in TUI by specifying a file that contains the initial machine state.

ELF Files aren't supported, but you can use the standard objdump tool to obtain a file similar to
the format that the simulator expects. You can find some examples in the `test` directory.
