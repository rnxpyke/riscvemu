{-# LANGUAGE FlexibleContexts #-}

module Emulator.Core where

import Data.STRef
import Data.Array.ST

import Control.Monad.Except
import Data.Word (Word32)

type Word5 = Int

data Error 
    = Err String
    | UnsupportedInstr
    | DecErr String deriving
    ( Eq, Show)

data MemRef
    = Pc
    | Reg Word5
    | Ram Word32

data Memory s = Memory
    { pc :: STRef s Word32
    , reg :: STArray s Word5 Word32
    , ram :: STArray s Word32 Word32
    }

class (Monad m, MonadError [Error] m) => MonadEmulator m where
    load  :: MemRef -> m Word32
    store :: MemRef -> Word32 -> m ()
    
type Decode inst m = Word32 -> m inst
type Execute inst m b = inst -> m b 


cycle :: MonadEmulator m 
    => Decode  i m
    -> Execute i m b
    -> m b
cycle decode execute = do
    pc <- load Pc
    bits <- load (Ram pc)
    store Pc (pc + 4)
    instr <- decode bits
    execute instr
