module Emulator.Init where


import Emulator.Core
import Control.Monad.ST
import Data.STRef
import Data.Array.ST

import Data.Word (Word32)

initPc :: ST s (STRef s Word32)
initPc = newSTRef 0

initReg ::  ST s (STArray s Word5 Word32)
initReg = newArray (0,31) 0

withLength :: Word32 -> ST s (Memory s)
withLength l = do
    pc <- initPc
    reg <- initReg
    ram <- newArray (0,l) 0
    return $ Memory pc reg ram

fromRam :: ST s (Memory s) 
fromRam = do
    pc <- initPc
    reg <- initReg
    ram <- newArray (0,1024) 0
    return $ Memory pc reg ram
