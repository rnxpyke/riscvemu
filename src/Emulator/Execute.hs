{-# LANGUAGE FlexibleContexts #-}

module Emulator.Execute where

import RiscV.RV32I
import Emulator.Core
import Emulator.Trans
import Control.Monad.Except
import Data.Bits
import Data.Word (Word32)

regToInt X0 = 0
regToInt X1 = 1
regToInt X2 = 2
regToInt X3 = 3
regToInt X4 = 4
regToInt X5 = 5
regToInt X6 = 6
regToInt X7 = 7
regToInt X8 = 8
regToInt X9 = 9
regToInt X10 = 10
regToInt X11 = 11
regToInt X12 = 12
regToInt X13 = 13
regToInt X14 = 14
regToInt X15 = 15
regToInt X16 = 16
regToInt X17 = 17
regToInt X18 = 18
regToInt X19 = 19
regToInt X20 = 20
regToInt X21 = 21
regToInt X22 = 22
regToInt X23 = 23
regToInt X24 = 24
regToInt X25 = 25
regToInt X26 = 26
regToInt X27 = 27
regToInt X28 = 28
regToInt X29 = 29
regToInt X30 = 30
regToInt X31 = 31

loadReg r = load (Reg $ regToInt r) 
storeReg r w = store (Reg $ regToInt r) w 

signExtendAt :: Int -> Word32 -> Word32
signExtendAt i w = if testBit w i
        then w .|. signMaskAt i
        else w 

signMaskAt :: Int -> Word32
signMaskAt i = flip shiftL i $ complement zeroBits

-- a bit buggy
extRange :: Integral a => (Int,Int) -> a -> Word32
extRange (lower,upper) i = signExtendAt (upper - 1) (shiftL word lower)
    where word = fromIntegral i



ext :: Word12 -> Word32
ext (Word12 w) = extRange (0,12) w

branchExt :: Word12 -> Word32
branchExt (Word12 w )= extRange (1,13) w 

jumpExt :: Word20 -> Word32
jumpExt (Word20 off) = extRange (1,21) off

class Executable a where
    execute :: 
        ( MonadEmulator m 
        , MonadError [Error] m
        ) => a -> m ()


instance Executable Instr where
    execute (BranchInstr i)      = execute i
    execute (CSRInstr i)         = execute i
    execute (EnvironmentInstr i) = execute i
    execute (JumpInstr i)        = execute i
    execute (MemoryInstr i)      = execute i
    execute (RIInstr i)          = execute i
    execute (RRInstr i)          = execute i
    execute (SyncInstr i)        = execute i

unsupported :: (MonadEmulator m, MonadError [Error] m) => m a
unsupported = throwError [UnsupportedInstr]

-- TODO
instance Executable RegisterRegisterInstr where
    execute (RInstr func rs2 rs1 rd) = do
        a1 <- loadReg rs1
        a2 <- loadReg rs2
        op <- f func
        storeReg rd (op a1 a2)
        where f :: (MonadError [Error] m, MonadEmulator m) => ROpcode -> m (Word32 -> Word32 -> Word32)
              f ADD  = pure (+)
              f SUB  = pure (-)
              f SLT  = unsupported 
              f SLTU = unsupported
              f AND  = pure (.&.)
              f OR   = pure (.|.)
              f XOR  = pure xor
              f SLL  = pure (\w i -> shiftL w (fromIntegral i))
              f SRL  = unsupported
              f SRA  = pure (\w i -> shiftR w (fromIntegral i))

instance Executable RegisterImmediateInstr where
    execute (IInstr func imm rs1 rd) = do
        a1 <- loadReg rs1
        op <- f func
        let a2 = ext imm
        storeReg rd (op a1 a2)
        where f ADDI = pure (+)
              f XORI = pure xor
              f ORI  = pure (.|.)
              f ANDI = pure (.&.)
              f SLTI = unsupported
              f SLTIU = unsupported
    execute (ShiftInstr func imm rs1 rd) = pure ()
    execute (LUI upper rd) = pure ()
    execute (AUIPC upper rd) = pure ()

instance Executable BranchInstr where
    execute (Branch offset cond rs2 rs1) = do
        a1 <- loadReg rs1
        a2 <- loadReg rs2
        f <- op cond
        if f a1 a2
        then do
            pc <- load Pc
            store Pc (pc + branchExt offset - 4) -- remove offset of cycle instr
        else pure ()
        where   op BEQ  = pure (==)
                op BNE  = pure (/=)
                op BLT  = unsupported
                op BLTU = pure (<)
                op BGE  = unsupported
                op BGEU = pure (>=)
        

instance Executable MemoryInstr where
    execute (LOAD (Width Word) offset base rd) = do
        addr <- loadReg base
        val <- load (Ram $ addr + ext offset)
        storeReg rd val
    execute (STORE Word offset rsrc base) = do
        addr <- loadReg base
        val <- loadReg rsrc
        store (Ram $ addr + ext offset) val
    execute (LOAD _ _ _ _) = unsupported
    execute (STORE _ _ _ _) = unsupported

instance Executable JumpInstr where
    execute (JAL off rd) = do
        pc <- load Pc
        storeReg rd pc
        store Pc (pc + jumpExt off - 4)
    execute (JALR off base rd) = do
        pc <- load Pc -- is already offset by 4 by cycle instruction
        storeReg rd pc
        addr <- loadReg base
        store Pc (addr + ext off - 4) -- remove offest for jump
                
instance Executable EnvironmentInstr where
    execute _ = unsupported

instance Executable CSRInstr where
    execute _ = unsupported

instance Executable SynchronizationInstr where
    execute _ = unsupported


