{-# LANGUAGE GeneralizedNewtypeDeriving, NamedFieldPuns, RankNTypes, ExistentialQuantification, FlexibleContexts #-}

module Emulator.Trans where

import Prelude hiding (STRef, readSTRef)

import Emulator.Core
import Emulator.Init

import Control.Monad.Reader
import Control.Monad.ST
import Control.Monad.Except
import Control.Monad.Identity
import Control.Applicative

import Data.STRef
import Data.Array.ST
import Data.Word (Word32)

newtype EmulatorT s m a = EmulatorT 
    { runEmulatorT :: ReaderT (Memory s) (ExceptT [Error] m) a
    } deriving 
    ( Functor, Applicative, Monad, Alternative
    , MonadReader (Memory s)
    , MonadError [Error]
    , MonadIO
    )

newtype STEmulator s a = STEmulator { runSTEmulator :: EmulatorT s (ST s) a }
    deriving 
    ( Functor, Applicative, Monad, Alternative
    , MonadReader (Memory s)
    , MonadError [Error]
    )

newtype IOEmulator a = IOEmulator { runIOEmulator :: EmulatorT RealWorld IO a }
    deriving
    ( Functor, Applicative, Monad, Alternative
    , MonadReader (Memory RealWorld)
    , MonadError [Error]
    , MonadIO
    )

loadMem :: Memory s -> MemRef -> ST s Word32
loadMem Memory{pc}   Pc     = readSTRef pc
loadMem Memory{reg} (Reg x) = readArray reg x
loadMem Memory{ram} (Ram x) = readArray ram x

storeMem :: Memory s -> MemRef -> Word32 -> ST s ()
storeMem Memory{pc}   Pc     v = writeSTRef pc v
storeMem Memory{reg} (Reg 0) v = pure ()
storeMem Memory{reg} (Reg x) v = writeArray reg x v
storeMem Memory{ram} (Ram x) v = writeArray ram x v


alignRam (Ram x) = do
    let (addr, rem) = quotRem x 4
    guard (rem == 0)
    return $ Ram addr
alignRam x = return x

loadHeave heave ref = do
    newref <- alignRam ref
    mem <- ask
    heave $ loadMem mem newref

storeHeave heave ref word = do
    newref <- alignRam ref
    mem <- ask
    heave $ storeMem mem newref word

instance MonadEmulator (STEmulator s) where
    load = loadHeave heave
        where heave = STEmulator . EmulatorT . lift . lift
    store = storeHeave heave    
        where heave = STEmulator . EmulatorT . lift . lift

instance MonadEmulator (IOEmulator) where
    load = loadHeave heave
        where heave = IOEmulator . EmulatorT . lift . lift . stToIO
    store = storeHeave heave    
        where heave = IOEmulator . EmulatorT . lift . lift . stToIO


execSTEmulator :: (forall s. ST s (Memory s))   -- inital memory
    -> (forall s. STEmulator s a)      -- emulator
    -> Either [Error] a                  -- result
execSTEmulator init emu = runST $ init >>= runExceptT . (runReaderT $ runEmulatorT $ runSTEmulator emu)

execSTinIO init emu = execIOEmulator init (emuToIO emu)

execIOEmulator :: Memory RealWorld -> IOEmulator a -> IO (Either [Error] a)
execIOEmulator init emu = runExceptT $ (runReaderT . runEmulatorT . runIOEmulator) emu init

emuToIO :: STEmulator RealWorld a -> IOEmulator a
emuToIO = runSTEmulator
    |> runEmulatorT
    |> runReaderT
    |> fmap (runExceptT |> stToIO |> ExceptT)
    |> ReaderT
    |> EmulatorT
    |> IOEmulator 
    where (|>) = flip (.)
