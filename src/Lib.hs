
module Lib where

import ISA
import Text.Read
import RiscV.RV32I
import RiscV.Decode.RV32I
import RiscV.Encode.RV32I

import Numeric

import Text.Regex.TDFA

import Control.Monad
import System.IO

decodeLines :: Handle -> IO [Maybe Instr]
decodeLines file = do
    contents <- hGetContents file
    let ls = lines contents
    return (readMaybe <$> ls)


showInst :: Maybe Instr -> IO ()
showInst Nothing = putStrLn "Nothing"
showInst (Just i) = putStrLn $ showHex (encodeInstr i) ""

convertInstructions :: FilePath -> IO ()
convertInstructions path = withFile path ReadMode $ \f -> do
    is <- decodeLines f
    forM_ is showInst

someFunc :: IO ()
someFunc = putStrLn "someFunc"
