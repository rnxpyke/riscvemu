{-# LANGUAGE NamedFieldPuns, FlexibleContexts #-}

module Tui where

import System.IO
import System.Environment

import Parse

import Prelude hiding (cycle)

import Brick.Main

import Emulator.Core
import Emulator.Trans
import Emulator.Init
import Emulator.Execute

import RiscV.RV32I (Instr)
import RiscV.Decode.RV32I

import Control.Monad.Trans
import Control.Monad.ST
import Control.Monad
import Control.Monad.Except
import Control.Monad.Reader

import System.Exit
import Data.STRef
import Data.Array.ST
import Data.Word (Word32)

import Brick.AttrMap
import Brick.Types

import Brick.Util
import Brick.Widgets.Core
import Brick.Widgets.Center

import Graphics.Vty.Attributes
import qualified Graphics.Vty as V

type Address = Word32

data State = State 
    { refs :: Memory RealWorld
    , regs :: [Word32]
    , drawpc :: Word32
    , ramview :: [(Address, Word32)]
    , exit :: Maybe [Error]
    }

app :: App State e String
app = App 
    { appDraw = draw
    , appChooseCursor = chooseCursor
    , appHandleEvent = handleEvent
    , appStartEvent = undefined
    , appAttrMap = attributeMap
    }


partition :: Int -> [a] -> [[a]]
partition x [] = []
partition x as = (take x as) : (partition x (drop x as))

renderWord :: Word32 -> Widget n
renderWord i = vLimit 1 $ hLimit 11 $ fill ' ' <+> str (show i)

renderRegs :: [Word32] -> Widget n
renderRegs regs = hBox $ fmap (vBox . fmap renderReg) iregs
    where iregs = partition 8 $ zip [0..31] regs
          renderReg (i,w) = value <+> index <+> str " | " 
            where index = vLimit 1 $ hLimit 4 $ (str $" :" ++  show i) <+> fill ' '
                  value = renderWord w


renderPc :: Word32 -> Widget n
renderPc pc = renderWord pc <+> str " :PC "


renderExit :: Maybe [Error] -> Widget n
renderExit Nothing = emptyWidget
renderExit (Just e) = hBox $ (str . show) <$> e


fillHorizontal :: Widget n
fillHorizontal = vLimit 1 (fill ' ')

renderRam :: [(Address, Word32)] -> Widget n
renderRam [] = str "hä"
renderRam list = hBox $ fmap (center . vBox . fmap single) (partition 16 list)
   where index i = vLimit 1 $ hLimit 14 $ (str $" :" ++  show i) <+> fill ' '
         renderInst :: Word32 -> Widget n
         renderInst i = either (const emptyWidget) (str . show) $ runExcept $ decodeInstr i
         single (address, word) = hBox 
                [ fillHorizontal
                , renderInst word 
                , str " " 
                , renderWord word 
                , index address
                ]

draw :: (Show n, Ord n) => State -> [Widget n]
draw State{regs, drawpc, exit, ramview} = pure $ (regs' <=> (pc' <+> exit')) <=> padTop (Pad 1) ramview'
    where   regs' = renderRegs regs
            pc' = renderPc drawpc
            exit' = renderExit exit
            ramview' = renderRam ramview

chooseCursor :: State -> [CursorLocation n] -> Maybe (CursorLocation n)
chooseCursor _ _ = Nothing

handleEvent :: State -> BrickEvent n e -> EventM n (Next (State))
handleEvent s ev = case ev of 
    VtyEvent (V.EvKey V.KEsc []) -> halt s
    VtyEvent (V.EvKey (V.KChar 'c') [V.MCtrl]) -> halt s
    VtyEvent (V.EvKey (V.KChar 'n') []) -> suspendAndResume (step s)
    _ -> continue s
    where dec :: MonadError [Error] m => Word32 -> m Instr
          dec w = either (\(DecodingError s) -> throwError [DecErr s]) pure $ runExcept $ decodeInstr w 
          step :: State -> IO State
          step s = fmap (either (const s) id) $ execIOEmulator (refs s) $ do
                   cycle dec execute
                   ask >>= (liftIO . stToIO . getState)
            `catchError` \e -> ask >>= (liftIO . stToIO . fmap (\s -> s {exit = Just e}) . getState)

startEvent :: State -> EventM n State
startEvent _ = liftIO $ stToIO (fromRam >>= getState)

attributeMap :: State -> AttrMap
attributeMap = const $ attrMap defAttr [(attrName "focused", fg red)]


getState ::  (Memory RealWorld) -> ST RealWorld State
getState mem = do
    drawpc <- readSTRef $ pc mem
    regs <- traverse (readArray $ reg mem) [0..31]
    let addr = drawpc `quot` 4
    let min = (max 8 addr) - 8
    let range = [min .. min + 15]
    ramview <- traverse (\i -> fmap ((,) (i * 4)) $ readArray (ram mem) i) range
    return State
        { refs = mem
        , drawpc
        , regs
        , ramview
        , exit = Nothing
        }

tuiMainInit :: (Memory RealWorld) -> IO ()
tuiMainInit init = defaultMain (app { appStartEvent }) undefined >> pure ()
    where appStartEvent :: State -> EventM s State
          appStartEvent = return $ liftIO $ stToIO $ getState init


tuiMain :: IO ()
tuiMain = do
    path <- head <$> getArgs
    mem <- withFile path ReadMode parseFile
    tuiMainInit mem

{-
tuiMain :: IO ()
tuiMain = defaultMain app s >> pure ()
    where s = State { refs = Nothing 
                    , regs = [1234567..1234567+31]
                    , drawpc = 0 }
-}
