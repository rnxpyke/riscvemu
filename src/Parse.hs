{-# LANGUAGE NamedFieldPuns #-}

module Parse where

import System.IO

import RiscV.RV32I
import RiscV.Decode.RV32I
import RiscV.Encode.RV32I

import Text.Regex.TDFA
import Text.Read

import Data.Semigroup (Max(..))
import Data.Maybe
import Data.Word (Word32)
import Data.STRef
import Data.Array.ST

import Control.Applicative
import Control.Monad
import Control.Monad.ST
import Control.Monad.Except

import Emulator.Core
import Emulator.Init
import ISA
import Numeric

readMap :: String -> Maybe (Word32, Word32)
readMap str = do
    (aStr,_,vStr) <- str =~~ ":" :: Maybe (String, String, String)
    addr <- readMaybe aStr
    val <- readHexWord vStr <|> readInstr vStr 
    return (addr, val) 

readInstr i = readMaybe i >>= Just . encodeInstr

readHexWord :: String -> Maybe Word32
readHexWord s = do
    num <- s =~~ "[0-9a-fA-F]+"
    readWord num
    where readWord = fmap fst . listToMaybe . readHex

    
zero :: Word32
zero = 0

parseFile :: Handle -> IO (Memory RealWorld)
parseFile fd = do
    ls <- lines <$> hGetContents fd
    let pcval = read $ head ls
    let mapping = tail ls >>= maybeToList . readMap
    let maxaddress = max (getMax $ foldMap (Max . fst) mapping) 1024
    mem <- stToIO (withLength maxaddress)
    stToIO $ writeSTRef (pc mem) pcval
    forM_ mapping $ \(add, word) -> stToIO $ writeArray (ram mem) (add `quot` 4) word  
    return mem
