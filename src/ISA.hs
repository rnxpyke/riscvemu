{-# LANGUAGE StandaloneDeriving #-}

module ISA where

import RiscV.RV32I
import RiscV.Encode.RV32I
import RiscV.Decode.RV32I

deriving instance Read Instr
deriving instance Read SynchronizationInstr
deriving instance Read SyncOrdering
deriving instance Read RegisterRegisterInstr
deriving instance Read Register
deriving instance Read RegisterImmediateInstr
deriving instance Read MemoryInstr
deriving instance Read JumpInstr
deriving instance Read EnvironmentInstr
deriving instance Read CSRInstr
deriving instance Read BranchInstr
deriving instance Read ROpcode
deriving instance Read Word5
deriving instance Read Word20
deriving instance Read Word12
deriving instance Read ShiftOpcode
deriving instance Read IOpcode
deriving instance Read Width
deriving instance Read LoadWidth
deriving instance Read CSRType
deriving instance Read CSRRegister
deriving instance Read BranchCond


